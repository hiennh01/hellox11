#include <X11/Xlib.h>
#include <assert.h>
#include <unistd.h>
#include <stdio.h>

int main()
{
	Display* dpy = XOpenDisplay(None);
	if(dpy)
		printf("DPY not null");
		printf("\nScreen number whatever: %d", XDefaultScreen(dpy));
		printf("\nVisual number whatever: %d", XDefaultVisual(dpy, XDefaultScreen(dpy)));
		printf("\nConnection number: %d", XConnectionNumber(dpy));
		printf("\nDefault depth: %d", XDefaultDepth(dpy, XDefaultScreen(dpy)));
		
		int n;
		int *listDepthArray = XListDepths(dpy, XDefaultScreen(dpy), &n);
		printf("\n\nListDepth number: %d", n);
		for(int i = 0; i < n; i++)
		{
			printf("\n  %d  %d", i, listDepthArray[i]);
		}
		DefaultScreen(dpy));
		
		printf("\nProtocolVersion: %d", XProtocolVersion(dpy));
		printf("\nProtocolRevision: %d", XProtocolRevision(dpy));
		printf("\nScreenCount: %d", XScreenCount(dpy));
		XCloseDisplay();
	}
	return 0;
}
