#include <X11/Xlib.h>
#include <assert.h>
#include <unistd.h>

#define NIL (0)

int main()
{
	//Open the display
	Display *dpy = XOpenDisplay(NIL);
	assert(dpy);

	//Get some colors
	int blackColor = BlackPixel(dpy, DefaultScreen(dpy));
	int whiteColor = WhitePixel(dpy, DefaultScreen(dpy));
	
	//Create the window
	//Window w = XCreateWindow(dpy, DefaultRootWindow(dpy), 0, 0, 200, 100, 0, CopyFromParent, CopyFromParent, CopyFromParent, NIL, 0);
	Window w = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy), 0, 0, 200, 100, 0, whiteColor, whiteColor);

	//We want to get MapNotify events
	XSelectInput(dpy, w, StructureNotifyMask);

	//Map the window
	XMapWindow(dpy, w);
	
	//Wait for the MapNotify event
	for(;;)
	{
		XEvent e;
		XNextEvent(dpy, &e);
		if(e.type == MapNotify)
			break;
	}

	//Draw the line
	GC gc = XCreateGC(dpy, w, 0, NIL);
	XDrawLine(dpy, w, gc, 10, 60, 180, 20);

	//
	XFlush(dpy);

	sleep(10);
}