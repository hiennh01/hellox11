#include <X11/Xlib.h>
#include <assert.h>
#include <unistd.h>
#include <stdio.h>

#define NIL (0)

int main()
{
	char keyboard_input[100];
	GC gc;
	XColor black_col, white_col, red_col, green_col, blue_col, yellow_col;
	Colormap colormap;
	char black_bits[] = "#000000";
	char white_bits[] = "#FFFFFF";
	char red_bits[] = "#FF0000";
	char green_bits[] = "#00FF00";
	char blue_bits[] = "#0000FF";
	char yellow_bits[] = "#FFFF00";

	Display *dpy = XOpenDisplay(NIL);
	assert(dpy);
	
	int blackColor = BlackPixel(dpy, DefaultScreen(dpy));
	int whiteColor = WhitePixel(dpy, DefaultScreen(dpy));
	
	//Define the colors we want to use
	colormap = DefaultColormap(dpy, 0);
	XParseColor(dpy, colormap, black_bits, &black_col);
	XAllocColor(dpy, colormap, &black_col);
	XParseColor(dpy, colormap, white_bits, &white_col);
	XAllocColor(dpy, colormap, &white_col);
	XParseColor(dpy, colormap, red_bits, &red_col);
	XAllocColor(dpy, colormap, &red_col);
	XParseColor(dpy, colormap, green_bits, &green_col);
	XAllocColor(dpy, colormap, &green_col);
	XParseColor(dpy, colormap, yellow_bits, &yellow_col);
	XAllocColor(dpy, colormap, &yellow_col);

	//Create the window. The numbers are the x and y locations on the screen,
	//the width and height, border width (which is usually zero)
	Window w = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy), 0, 0, 200, 100, 0, blackColor, blackColor);
	XSelectInput(dpy, w, StructureNotifyMask);
	XMapWindow(dpy, w);
	for(;;)
	{
		XEvent e;
		XNextEvent(dpy, &e);
		if(e.type == MapNotify)
		{
			break;
		}
	}
	
	//Which means that the window has appeared on the screen.
	gc = XCreateGC(dpy, w, 0, NIL);
	
	//We are finally ready to do some drawing! Whew!
	XSetForeground(dpy, gc, red_col.pixel);
	XDrawLine(dpy, w, gc, 10, 70, 180, 30);
	XSetForeground(dpy, gc, green_col.pixel);
	XDrawLine(dpy, w, gc, 10, 70, 180, 40);
	XSetForeground(dpy, gc, blue_col.pixel);
	XDrawLine(dpy, w, gc, 10, 70, 180, 50);
	XSetForeground(dpy, gc, white_col.pixel);
	XDrawLine(dpy, w, gc, 10, 70, 180, 20);
	
	XSetForeground(dpy, gc, green_col.pixel);
	XDrawArc(dpy, w, gc, 150, 50, 40, 40, 90*64, 180*64);
	XSetForeground(dpy, gc, red_col.pixel);
	XDrawArc(dpy, w, gc, 155, 55, 30, 30, 180*64, 180*64);

	// Lets try 
	XSetForeground(dpy, gc, red_col.pixel);	
	XDrawRectangle(dpy, w, gc, 10, 10, 50, 50);
	
	//Lets try
	XSetForeground(dpy, gc, blue_col.pixel);
	XFillRectangle(dpy, w, gc, 15, 15, 40, 40);

	XSetForeground(dpy, gc, black_col.pixel);
	XFillArc(dpy, w, gc, 20, 20, 30, 30, 10*64, 330*64);
	XSetForeground(dpy, gc, yellow_col.pixel);
	XFillArc(dpy, w, gc, 37, 23, 6, 6, 0*64, 360*64);

	XFlush(dpy);
	sleep(1);
	printf("Press enter when done.\n");
	fgets(keyboard_input, 100, stdin);
	return(0);
}

















