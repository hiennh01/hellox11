#include <X11/Xlib.h>
#include <Imlib2.h>
#include <stdio.h>

Display *disp;
Window win;
Visual *vis;
Colormap cm;
int depth;

int main(int argc, char **argv)
{
	XEvent ev;
	disp = XOpenDisplay(NULL);
	vis = DefaultVisual(disp, DefaultScreen(disp));
	depth = DefaultDepth(disp, DefaultScreen(disp));
	cm = DefaultColormap(disp, DefaultScreen(disp));
	
	win = XCreateSimpleWindow(disp, DefaultRootWindow(disp), 0, 0, 640, 480, 0, 0, 0);
	XSelectInput(disp, win, ButtonPressMask | ButtonReleaseMask | PointerMotion | ExposureMask);
	XMapWindow(disp, win);
	
	Imlib_Image image;
	
	imlib_context_set_display(disp);
	imlib_context_set_visual(vis);
	imlib_context_set_colormap(cm);
	imlib_context_set_drawable(win);
	
	while (1) 
	{
		XNextEvent(disp, &ev);
	}
	return 0;
}
