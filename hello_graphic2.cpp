#include <X11/Xlib.h>
#include <assert.h>
#include <stdio.h>
#include <unistd.h>

#define NIL (0)

int main()
{
	char keyboard_input[100];
	Display *dpy = XOpenDisplay(NIL);
	assert(dpy);
	
	int blackColor = BlackPixel(dpy, DefaultScreen(dpy));
	int whiteColor = WhitePixel(dpy, DefaultScreen(dpy));

	Window window = XCreateSimpleWindow(dpy, DefaultRootWindow(dpy), 0, 0, 200, 100, 0, whiteColor, whiteColor);
	XSelectInput(dpy, window, StructureNotifyMask);
	XMapWindow(dpy, window);
	
	for(;;)
	{
		XEvent e;
		XNextEvent(dpy, &e);
		if(e.type == MapNotify)
		{
			break;
		}
	}

	XFlush(dpy);

	sleep(5);

	GC gc = XCreateGC(dpy, window, 0, NIL);
	XFillArc(dpy, window, gc, 15, 15, 40, 40, 10*64, 330*64);
	XFlush(dpy);

	printf("Press enter when done.\n");
	fgets(keyboard_input, 100, stdin);

	return 0;
}