#include <X11/Xlib.h>
#include <Imlib2.h>
#include <stdio.h>

/* some globals for our window & X display */
Display *disp;
Window   win;
Visual  *vis;
Colormap cm;
int      depth;

int main(int argc, char **argv)
{
	/* events we get from X */
	XEvent ev;
	/* areas to update */
	Imlib_Updates updates, current_update;
	/* our virtual framebuffer image we draw into */
	Imlib_Image buffer;

	/* connect to X */
	disp  = XOpenDisplay(NULL);
	vis   = DefaultVisual(disp, DefaultScreen(disp));
	depth = DefaultDepth(disp, DefaultScreen(disp));
	cm    = DefaultColormap(disp, DefaultScreen(disp));
	win   = XCreateSimpleWindow(disp, DefaultRootWindow(disp), 0, 0, 640, 480, 0, 0, 0);
	
	XSelectInput(disp, win, ButtonPressMask | ButtonReleaseMask | PointerMotionMask | ExposureMask);
	XMapWindow(disp, win);
	
	/* set our cache to 2 Mb so it doesn't have to go hit the disk as long as */
	/* the images we use use less than 2Mb of RAM (that is uncompressed) */
	imlib_set_cache_size(2048 * 1024);
	/* set the font cache to 512Kb - again to avoid re-loading */
	imlib_set_font_cache_size(512 * 1024);
	/* add the ./ttfonts dir to our font path - you'll want a notepad.ttf */
	/* in that dir for the text to display */
	imlib_add_path_to_font_path("./ttfonts");
	/* set the maximum number of colors to allocate for 8bpp and less to 128 */
	imlib_set_color_usage(128);
	/* dither for depths < 24bpp */
	imlib_context_set_dither(1);
	/* set the display , visual, colormap and drawable we are using */
	imlib_context_set_display(disp);
	imlib_context_set_visual(vis);
	imlib_context_set_colormap(cm);
	imlib_context_set_drawable(win);
	
	
	Imlib_Image image;
	/* infinite event loop */
	for (;;)
	{
		/* init our updates to empty */
		updates = imlib_updates_init();
		/* while there are events form X - handle them */
		do
		{
			XNextEvent(disp, &ev);
			switch (ev.type)
			{
				case Expose:
					printf("This is Expose");
					/* window rectangle was exposed - add it to the list of */
					/* rectangles we need to re-render */
					updates = imlib_update_append_rect(updates,
					ev.xexpose.x, ev.xexpose.y,
					ev.xexpose.width, ev.xexpose.height);
					break;
					
				case ButtonPress:
					printf("This is ButtonPress");
					return 1;
					break;
			}
		} while (XPending(disp));
		
		image = imlib_load_image("../testdata/cartoon3.jpg");
		imlib_context_set_image(image);
		imlib_render_image_on_drawable(0, 0);
		imlib_free_image();
	}
	return 0;
}
