#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <X11/Xlib.h>

XImage *CreateTrueColorImage(Display *display, Visual *visual, unsigned char *image, int width, int height)
{
	int i,j;
	unsigned char *image32=(unsigned char *)malloc(width*height*4);
	unsigned char *p=image32;
	for(i=0; i<width; i++)
	{
		for(j=0; j<height; j++)
		{
			if((i<256) && (j<256))
			{
				*p++ = rand()%256;
				*p++ = rand()%256;
				*p++ = rand()%256;				
			}
			else
			{
				*p++ = i%256;
				*p++ = j%256;
				if(i<256)
				{
					*p++ = i%256;
				}
				else if(j<256)
				{
					*p++ = j%256;
				}
				else
				{
					*p++ = (256-j)%256;
				}
			}
			p++;
		}
	}
	return XCreateImage(display, visual, 24, ZPixmap, 0, (char*)image32, width, height, 32, 0);
}

void processEvent(Display *display, Window window, XImage *ximage, int width, int height)
{
	static char *tir="This is red";
	static char *tig="This is green";
	static char *tib="This is blue";
	XEvent ev;
	XNextEvent(display, &ev);
	switch(ev.type)
	{
	case Expose:
		XPutImage(display, window, DefaultGC(display, 0), ximage, 0, 0, 0, 0, width, height);
		XSetForeground(display, DefaultGC(display, 0), 0x00ff0000);
		XDrawString(display, window, DefaultGC(display, 0), 32, 32, tir, strlen(tir));
		XDrawString(display, window, DefaultGC(display, 0), 32+256, 32, tir, strlen(tir));
		XDrawString(display, window, DefaultGC(display, 0), 32+256, 32+256, tir, strlen(tir));
		XDrawString(display, window, DefaultGC(display, 0), 32, 32+256, tir, strlen(tir));

		XSetForeground(display, DefaultGC(display, 0), 0x0000ff00);
		XDrawString(display, window, DefaultGC(display, 0), 32, 52, tig, strlen(tig));
		XDrawString(display, window, DefaultGC(display, 0), 32+256, 52, tig, strlen(tig));
		XDrawString(display, window, DefaultGC(display, 0), 32+256, 52+256, tig, strlen(tig));
		XDrawString(display, window, DefaultGC(display, 0), 32, 52+256, tig, strlen(tig));

		XSetForeground(display, DefaultGC(display, 0), 0x000000ff);
		XDrawString(display, window, DefaultGC(display, 0), 32, 72, tib, strlen(tib));
		XDrawString(display, window, DefaultGC(display, 0), 32+256, 72, tib, strlen(tib));
		XDrawString(display, window, DefaultGC(display, 0), 32+256, 72+256, tib, strlen(tib));
		XDrawString(display, window, DefaultGC(display, 0), 32, 72+256, tib, strlen(tib));
	case ButtonPress:
		exit(0);
	}
}

int main(int argc, char **argv)
{
	/*unsigned long black, white;
	dis = XOpenDisplay((char *) 0);
	screen = DefaultScreen(dis);
	black = BlackPixel(dis, screen);
	white = WhitePixel(dis, screen);
	
	//win = XCreateSimpleWindow(dis, RootWindow(dis, screen), 10, 10, 660, 200, 1, BlackPixel(dis, screen), WhitePixel(dis, screen));
	win = XCreateSimpleWindow(dis, DefaultRootWindow(dis), 0, 0, 200, 300, 5, white, black);
	XMapWindow(dis, win);
	XStoreName (dis, win, "Geeks3D.com - X11 window under Linux (Mint 10)");
	XInternAtom(dis, "WM_DELETE_WINDOW", False);*/


	XImage *ximage;
	int width=512, height=512;
	Display *display=XOpenDisplay((char *) 0);
	int screen=DefaultScreen(display);
	Visual *visual=DefaultVisual(display, 0);
	Window window = XCreateSimpleWindow(display, RootWindow(display, screen), 0, 0, width, height, 1, 0, 0);
	
	printf("Chay toi day\n");
	ximage=CreateTrueColorImage(display, visual, (unsigned char*)0, width, height);
	XSelectInput(display, window, ButtonPressMask|ExposureMask);
	XMapWindow(display, window);

	XStoreName (display, window, "Geeks3D.com - X11 window under Linux (Mint 10)");
	XInternAtom(display, "WM_DELETE_WINDOW", False);

	while(1)
	{
		//printf("lala\n");
//		processEvent(display, window, ximage, width, height);
	}
	return 0;
}