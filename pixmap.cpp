#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>
#include <unistd.h>

void drawPixmap()
{
	Pixmap bitmap;
	unsigned int bitmap_width, bitmap_height;
	int hotspot_x, hotspot_y;
	Window root_win = DefaultRootWindow(display);
	
	int rc = XReadBitmapFile(display, root_win, "icon.bmp", &bitmap_width, &bitmap_height, &bitmap, &hotspot_x, &hotspot_y);
	switch(rc)
	{
		case BitmapOpenFailed:
			printf("Could not open file.");
			break;
		case BitmapFileInvalid:
			printf("File does not contain a valid bitmap.")
			break;
		case BitmapNoMemory:
			printf("Not enough memory.");
			break;
		case BitmapSuccess:
			printf("Load successfully");
			break;
	}
}

int main()
{
	Display *dis;
	Window win;
	dis = XOpenDisplay(NULL);
	win = XCreateSimpleWindow(dis, RootWindow(dis, 0), 1, 1, 500, 500, 0, BlackPixel(dis, 0), BlackPixel(dis, 0));
	XMapWindow(dis, win);
	XFlush(dis);
	sleep(5);
	return 0;
}
