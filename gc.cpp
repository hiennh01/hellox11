#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <jpeglib.h>
#include <jerror.h>

int get_byte_order(void)
{
	union {
		char c[sizeof(short)];
		short s;
	} order;
	
	order.s = 1;
	if ((1 == order.c[0])) {
		return LSBFirst;
	} else {
		return MSBFirst;
	}
}

XImage *create_image_from_buffer(Display *dis, int screen, unsigned char *buf, int width, int height)
{
	int depth;
	XImage *img = NULL;
	Visual *vis;
	double rRatio;
	double gRatio;
	double bRatio;
	int outIndex = 0;
	int i;
	int numBufBytes = (3 * (width*height));
	
	depth = DefaultDepth(dis, screen);
	vis = DefaultVisual(dis, screen);
	
	rRatio = vis->red_mask / 255.0;
	gRatio = vis->green_mask / 255.0;
	bRatio = vis->blue_mask / 255.0;
	
	if(depth >= 24)
	{
		size_t numNewBufBytes = (4*(width * height));
		uint32_t *newBuf = (uint32_t*)malloc(numNewBufBytes);
		if(NULL == newBuf)
		{
			perror("malloc");
			return NULL;
		}
		
		for(i = 0; i < numBufBytes; ++i)
		{
			unsigned int r, g, b;
			r = (buf[i] * rRatio);
			++i;
			g = (buf[i] * gRatio);
			++i;
			b = (buf[i] * bRatio);
			
			r &= vis->red_mask;
			g &= vis->green_mask;
			b &= vis->blue_mask;
			
			newBuf[outIndex] = r | g | b;
			++outIndex;
		}
		
		img = XCreateImage(dis, CopyFromParent, depth, ZPixmap, 0, (char *) newBuf, width, height, 32, 0);
		
	}
	else if (depth >= 15) 
	{
		size_t numNewBufBytes = (2 * (width * height));
		uint16_t *newBuf = (uint16_t*)malloc(numNewBufBytes);
		
		if(NULL == newBuf)
		{
			perror("malloc");
			return NULL;
		}
		
		for(i = 0; i < numBufBytes; ++i)
		{
			unsigned int r, g, b;
			r = (buf[i] * rRatio);
			++i;
			g = (buf[i] * gRatio);
			++i;
			b = (buf[i] * bRatio);
			
			r &= vis->red_mask;
			g &= vis->green_mask;
			b &= vis->blue_mask;
			
			newBuf[outIndex] = r | g | b;
			++outIndex;
		}
		
		img = XCreateImage(dis, CopyFromParent, depth, ZPixmap, 0, (char *) newBuf, width, height, 16, 0);
		
	} 
	else
	{
		printf("This program does not support displays with a depth less then 15");
		return NULL;
	}
	
	if(NULL == img)
	{
		return NULL;
	}
	
	XInitImage(img);
	if((LSBFirst == get_byte_order()))
	{
		img->byte_order = LSBFirst;
	}
	else
	{
		img->byte_order = MSBFirst;
	}
	
	img->bitmap_bit_order = MSBFirst;
	return img;
}

void jpeg_error_exit(j_common_ptr cinfo)
{
	cinfo->err->output_message(cinfo);
	exit (EXIT_FAILURE);
}

unsigned char *decode_bmp(char *filename, int *widthPtr, int *heightPtr)
{
	int i;
	FILE* f = fopen(filename, "rb");
	unsigned char info[54];
	fread(info, sizeof(unsigned char), 54, f);
	
	//extract image height and width from header
	*widthPtr = *(int*)&info[18];
	*heightPtr = *(int*)&info[22];
	
	int size = 3 * *widthPtr * *heightPtr;
	unsigned char* data = new unsigned char[size];
	fread(data, sizeof(unsigned char), size, f);
	fclose(f);
	
	for(i = 0; i < size; i += 3)
	{
		unsigned char tmp = data[i];
		data[i] = data[i+2];
		data[i+2] = tmp;
	}
	
	return data;
}

unsigned char *decode_jpeg(char *filename, int *widthPtr, int *heightPtr)
{
	register JSAMPARRAY lineBuf;
	struct jpeg_decompress_struct cinfo;
	struct jpeg_error_mgr err_mgr;
	int bytesPerPix;
	FILE *inFile;
	unsigned char *retBuf;
	
	inFile = fopen(filename, "rb");
	if (NULL == inFile)
	{
		perror(NULL);
		return NULL;
	}
	
	cinfo.err = jpeg_std_error(&err_mgr);
	err_mgr.error_exit = jpeg_error_exit;
	
	jpeg_create_decompress(&cinfo);
	jpeg_stdio_src(&cinfo, inFile);
	jpeg_read_header(&cinfo, 1);
	cinfo.do_fancy_upsampling = 0;
	cinfo.do_block_smoothing = 0;
	jpeg_start_decompress(&cinfo);
	
	*widthPtr = cinfo.output_width;
	*heightPtr = cinfo.output_height;
	bytesPerPix = cinfo.output_components;
	
	lineBuf = cinfo.mem->alloc_sarray((j_common_ptr)&cinfo, JPOOL_IMAGE, (*widthPtr * bytesPerPix), 1);
	int size = *widthPtr * *heightPtr;
	retBuf = (unsigned char*)malloc(3 * size);
	
	if(NULL == retBuf)
	{
		perror(NULL);
		return NULL;
	}
	
	if(3 == bytesPerPix)
	{
		int lineOffset = (*widthPtr * 3);
		int x;
		int y;
		
		for(y = 0; y < cinfo.output_height; ++y)
		{
			jpeg_read_scanlines(&cinfo, lineBuf, 1);
			
			for(x = 0; x < lineOffset; ++x)
			{
				retBuf[(lineOffset * y) + x] = lineBuf[0][x];
				++x;
				retBuf[(lineOffset * y) + x] = lineBuf[0][x];
				++x;
				retBuf[(lineOffset * y) + x] = lineBuf[0][x];
			}
		}
	}
	else if (1 == bytesPerPix) 
	{
		unsigned int col;
		int lineOffset = (*widthPtr * 3);
		int lineBufIndex;
		int x;
		int y;
		
		for(y = 0; y < cinfo.output_height; ++y)
		{
			jpeg_read_scanlines(&cinfo, lineBuf, 1);
			lineBufIndex = 0;
			for(x=0; x<lineOffset; ++x)
			{
				col = lineBuf[0][lineBufIndex];
				retBuf[(lineOffset * y) + x] = col;
				++x;
				retBuf[(lineOffset * y) + x] = col;
				++x;
				retBuf[(lineOffset * y) + x] = col;
				
				++lineBufIndex;
			}
		}
	}
	else
	{
		printf("Error");
	}
	jpeg_finish_decompress (&cinfo);
	jpeg_destroy_decompress (&cinfo);
	fclose (inFile);
	
	return retBuf;
}

int main()
{
	char *display_name = getenv("DISPLAY");
	Display *display = XOpenDisplay(display_name);
	Window win;
	if(display == NULL)
	{
		printf("There are some error.");
	}
	
	//Create simple window
	int win_border_width = 2;
	win = XCreateSimpleWindow(display, RootWindow(display, DefaultScreen(display)), 0, 0, 
							  DisplayWidth(display, DefaultScreen(display)), 
							  DisplayHeight(display, DefaultScreen(display)),
							  win_border_width, BlackPixel(display, DefaultScreen(display)),
							  WhitePixel(display, DefaultScreen(display)));
	XSelectInput(display, win, StructureNotifyMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | ExposureMask);
	XMapWindow(display, win);
	XFlush(display);
	
	//Wait for the MapNotify event
	XEvent e;
	for(;;)
	{
		XNextEvent(display, &e);
		if(e.type == MapNotify)
			break;
	}
	
	//Create new GC
	unsigned long valuemask = 0;
	XGCValues values;
	GC gc = XCreateGC(display, win, valuemask, &values);
	XSetForeground(display, gc, WhitePixel(display, DefaultScreen(display)));
	XSetBackground(display, gc, BlackPixel(display, DefaultScreen(display)));
	XSetLineAttributes(display, gc, 2, LineSolid, CapButt, JoinBevel);
	XSetFillStyle(display, gc, FillSolid);
		
	//Color declare
	Colormap screen_colormap = DefaultColormap(display, DefaultScreen(display));
	XColor red, brown, blue, yellow, green;
	Status rc;
	rc = XAllocNamedColor(display, screen_colormap, "red", &red, &red);
	rc = XAllocNamedColor(display, screen_colormap, "brown", &brown, &brown);
	rc = XAllocNamedColor(display, screen_colormap, "blue", &blue, &blue);
	rc = XAllocNamedColor(display, screen_colormap, "yellow", &yellow, &yellow);
	rc = XAllocNamedColor(display, screen_colormap, "green", &green, &green);
		
	//Draw something
	int width = DisplayWidth(display, DefaultScreen(display));
	int height = DisplayHeight(display, DefaultScreen(display));
	XSetForeground(display, gc, red.pixel);
	XDrawPoint(display, win, gc, 150, 150);
	XDrawPoint(display, win, gc, 150, height - 150);
	XDrawPoint(display, win, gc, width - 150, 150);
	XDrawPoint(display, win, gc, width - 150, height - 150);
		
	XSetForeground(display, gc, brown.pixel);
	XDrawLine(display, win, gc, 50, 0, 50, 200);
	XDrawLine(display, win, gc, 0, 100, 200, 100);
	
	XSetForeground(display, gc, blue.pixel);
	XDrawArc(display, win, gc, 50-(30/2), 100-(30/2), 30, 30, 0, 360*64);
	
	XPoint points[] = {	{0, 0}, {15, 15}, {0, 15}, {0, 0}};
	int npoints = sizeof(points)/sizeof(XPoint);
	XSetForeground(display, gc, yellow.pixel);
	XDrawLines(display, win, gc, points, npoints, CoordModeOrigin);
	
	XSetForeground(display, gc, BlackPixel(display, DefaultScreen(display)));
	XDrawRectangle(display, win, gc, 120, 150, 50, 60);
	
	XSetForeground(display, gc, green.pixel);
	XFillRectangle(display, win, gc, 60, 150, 50, 60);
	
	XFlush(display);
	
	//Draw image
	unsigned char *buf;
	int imageWidth;
	int imageHeight;
	buf = decode_jpeg("../testdata/cartoon1.jpg", &imageWidth, &imageHeight);
	XImage *image = create_image_from_buffer(display, DefaultScreen(display), buf, imageWidth, imageHeight);
	free(buf);
	GC copyGC = XCreateGC(display, win, 0, NULL);
	//XPutImage(display, win, copyGC, image, 0, 0, 0, 0, imageWidth, imageHeight);
	
	//Draw image2
	unsigned char *buf2;
	int imageWidth2;
	int imageHeight2;
	buf2 = decode_jpeg("../testdata/cartoon2.jpg", &imageWidth2, &imageHeight2);
	XImage *image2 = create_image_from_buffer(display, DefaultScreen(display), buf2, imageWidth2, imageHeight2);
	free(buf2);
	GC copyGC2 = XCreateGC(display, win, 0, NULL);
	//XPutImage(display, win, copyGC2, image2, 100, 100, 0, 0, imageWidth2, imageHeight2);
	
	//Fade effect
	printf("\nGia tri widthh %d height %d", image2->width, image2->height);
	printf("\nByte per line: %d", image->bytes_per_line);
	printf("\nByte per line: %d", image2->bytes_per_line);
	printf("\nDepth: %d", image->depth);
	printf("\nDepth: %d", image2->depth);
	for(int j = 0; j < image2->height; j++)
	{		
		for(int i = 0; i < image2->bytes_per_line; i++)	
		{
			/*image2->data[j*image2->bytes_per_line + i] = image->data[j*image->bytes_per_line + i];
			image2->data[j*image2->bytes_per_line + i+1] = image->data[j*image->bytes_per_line + i+1];
			image2->data[j*image2->bytes_per_line + i+2] = image->data[j*image->bytes_per_line + i+2];
			image2->data[j*image2->bytes_per_line + i+3] = image->data[j*image->bytes_per_line + i+3];*/
			image2->data[j*image2->bytes_per_line + i] = float(float(image2->data[j*image2->bytes_per_line + i]) + image->data[j*image->bytes_per_line + i]) / 2.0;
			image2->data[j*image2->bytes_per_line + i+1] = float(float(image2->data[j*image2->bytes_per_line + i+1]) + image->data[j*image->bytes_per_line + i+1]) / 2.0;
			image2->data[j*image2->bytes_per_line + i+2] = float(float(image2->data[j*image2->bytes_per_line + i+2]) + image->data[j*image->bytes_per_line + i+2]) / 2.0;
			image2->data[j*image2->bytes_per_line + i+3] = float(float(image2->data[j*image2->bytes_per_line + i+3]) + image->data[j*image->bytes_per_line + i+3]) / 2.0;
			//image2->data[j*image2->bytes_per_line + i + 2] = image2->data[j*image2->bytes_per_line + i];
			i = i + 3;
		}
	}
	XPutImage(display, win, copyGC2, image2, 100, 100, 0, 0, imageWidth2, imageHeight2);
	
	//Draw image3
	unsigned char *buf3;
	int imageWidth3;
	int imageHeight3;
	buf3 = decode_bmp("../testdata/Bliss.bmp", &imageWidth3, &imageHeight3);
	XImage *image3 = create_image_from_buffer(display, DefaultScreen(display), buf3, imageWidth3, imageHeight3);
	free(buf3);
	GC copyGC3 = XCreateGC(display, win, 0, NULL);
	XPutImage(display, win, copyGC3, image3, 100, 100, 0, 0, imageWidth3, imageHeight3);
	
	/*// Draw Pixmap
	Pixmap bitmap;
	unsigned int bitmap_width, bitmap_height;
	int hotspot_x, hotspot_y;
	rc = XReadBitmapFile(display, win, "icon.bmp", &bitmap_width, &bitmap_height, &bitmap, &hotspot_x, &hotspot_y);
	switch(rc) {
		case BitmapOpenFailed:
		case BitmapFileInvalid:
		case BitmapNoMemory:
			printf("Error!!!!");
			break;
		case BitmapSuccess:
			int i, j;
			for(i=0; i<6; i++)
			{
				for(j=0; j<6; j++)
				{
					XCopyPlane(display, bitmap, win, gc, 0, 0, bitmap_width, bitmap_height, j*bitmap_width, i*bitmap_height, 1);
					XSync(display, False);
					sleep(1);
				}
			}
			break;
	}*/
		
	// Control event
	XEvent ev;
	for(;;)
	{
		XNextEvent(display, &ev);
		switch(ev.type)
		{
			case ButtonPress:
				if(0<ev.xmotion.x && ev.xmotion.x<20 && 0<ev.xmotion.y && ev.xmotion.y<20)
				{
					exit(0);
				}
				printf("\nClick Mouse position: %d %d", ev.xmotion.x, ev.xmotion.y);
				break;
			
			case MotionNotify:
				printf("\nMouse position: %d %d", ev.xmotion.x, ev.xmotion.y);
				break;
				
			case Expose:
				XPutImage(display, win, copyGC2, image3, 100, 100, 0, 0, imageWidth3, imageHeight3);
				break;
		}
	}
	
	//Close display and all created window
	XCloseDisplay(display);
	return 0;
}
