#include <X11/Xlib.h>
#include <Imlib2.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char** argv)
{
	Imlib_Image image;

	printf("Gia tri so phan tu:%d",argc);	

	if(argc != 3)
		return 1;
	//load image
	image = imlib_load_image(argv[1]);
	if(image)
	{
		printf("Chay vao day");
		char *tmp;
		imlib_context_set_image(image);
		tmp = strrchr(argv[2], '.');
		if(tmp)
		{
			imlib_image_set_format(tmp + 1);
		}
		imlib_save_image(argv[2]);
	}
	return 0;
}